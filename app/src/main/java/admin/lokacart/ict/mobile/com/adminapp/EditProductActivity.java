package admin.lokacart.ict.mobile.com.adminapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class EditProductActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText eProductName, eProductQuantity, eProductPrice,eProductDescription;
    Button bProductSave, bProductCancel, bProductUploadImage, bProductUploadAudio, bSave, bCancel;
    ImageView ivProduct;
    private String imgPath;
    //ArrayList<String> productList, priceList, quantityList;
    int position, recyclerViewIndex;
    final int REQUEST_GALLERY = 2, REQUEST_CAMERA = 1, REQUEST_AUDIO = 3;
    float dpHeight, dpWidth;
    private static MediaRecorder mediaRecorder;
    private static MediaPlayer mediaPlayer;
    private static String recordedAudioFilePath, selectedAudioFilePath, capturedImageFilePath, selectedImageFilePath;

    public String tempImage, tempAudio,fromWhere,productName,productDescription,productId;
    public int stockQuantity,productTypePos,productPos;
    public Double productUnitPrice;
    Random randomno;
    SharedPreferences sharedPreferences;
    TextView tDescCharRemaining,tProductNameCharRemaining;
    Button bPlay, bStop, bRecord, bUpload;
    private boolean isRecording = false, isPlaying = false, isRecorded = false;
    private static final String TAG = "EditProductActivity";

    File file, file1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Master.getAdminData(getApplicationContext());
        setContentView(R.layout.activity_edit_product);
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        randomno = new Random();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        try
        {
            /*productList = getIntent().getStringArrayListExtra("productList");
            priceList = getIntent().getStringArrayListExtra("priceList");
            quantityList = getIntent().getStringArrayListExtra("quantityList");*/
            position = getIntent().getIntExtra("position", 0);
            recyclerViewIndex = getIntent().getIntExtra("size", 0);
            fromWhere=getIntent().getStringExtra("fromWhere");
            productName=getIntent().getStringExtra("product_name");
            productDescription=getIntent().getStringExtra("description");
            productUnitPrice=getIntent().getDoubleExtra("unitPrice", 0);
            stockQuantity=getIntent().getIntExtra("quantity", 0);
            productId=getIntent().getStringExtra("product_id");
            productTypePos=getIntent().getIntExtra("product_type_pos", 0);
            productPos=getIntent().getIntExtra("product_pos", 0);
          //  System.out.println("stockQuantity:"+stockQuantity);
          //  System.out.println("description:"+productDescription);

            //productName = productList.get(position);
            eProductName = (EditText) findViewById(R.id.eProductName);
            //eProductName.setText(productList.get(position));
            eProductPrice = (EditText) findViewById(R.id.eProductPrice);
            //eProductPrice.setText(priceList.get(position));
            eProductQuantity = (EditText) findViewById(R.id.eProductQuantity);
            //eProductQuantity.setText(quantityList.get(position));
            eProductDescription=(EditText) findViewById(R.id.eProductDescription);
            tDescCharRemaining = (TextView) findViewById(R.id.tDescCharRemaining);
            tProductNameCharRemaining = (TextView)findViewById(R.id.tProductNameCharRemaining);

            if(fromWhere != null && fromWhere.equals("fromDashboardActivity")){
                eProductName.setText(productName);
                eProductPrice.setText(productUnitPrice.toString());
                eProductQuantity.setText(stockQuantity + "");
                eProductDescription.setText(productDescription);

            }else {
                eProductName.setText(Master.productList.get(position));
                eProductPrice.setText(Master.priceList.get(position));
                eProductQuantity.setText(Master.quantityList.get(position));
                eProductDescription.setText(Master.descList.get(position));
                productName = Master.productList.get(position);
            }
            setTitle(productName);
            tDescCharRemaining.setText((500 - eProductDescription.length()) + " " + getString(R.string.textview_characters_left));
            tProductNameCharRemaining.setText((50 - eProductName.length()) + " " + getString(R.string.textview_characters_left));


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        eProductDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != -1)
                    tDescCharRemaining.setText((500 - s.length()) + " " + getString(R.string.textview_characters_left));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        eProductName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != -1)
                    tProductNameCharRemaining.setText((50 - s.length()) + " " + getString(R.string.textview_characters_left));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        bProductSave = (Button) findViewById(R.id.bProductSave);
        bProductSave.setOnClickListener(this);

        bProductCancel = (Button) findViewById(R.id.bProductCancel);
        bProductCancel.setOnClickListener(this);

        bProductUploadImage = (Button) findViewById(R.id.bProductUploadImage);
        bProductUploadImage.setOnClickListener(this);

        bProductUploadAudio = (Button) findViewById(R.id.bProductUploadAudio);
        bProductUploadAudio.setOnClickListener(this);

        DisplayMetrics displayMetrics = EditProductActivity.this.getResources().getDisplayMetrics();
        dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        dpWidth = displayMetrics.widthPixels / displayMetrics.density;
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bProductSave:
                save();
                break;

            case R.id.bProductCancel:
                finish();
                break;

            case R.id.bProductUploadImage:
                uploadImage();
                break;

            case R.id.bProductUploadAudio:
                uploadAudio();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);
        Master.getAdminData(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mediaRecorder != null){
            mediaRecorder.stop();
        }
    }

    private void uploadImage()
    {
        final CharSequence[] options = { getString(R.string.upload_image_take_photo), getString(R.string.upload_image_choose_from_gallery),getString(R.string.label_button_cancel) };
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProductActivity.this);
        builder.setTitle(R.string.upload_image_title);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    tempImage = "temp" + randomno.nextInt(1000000) + ".jpg";
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), tempImage);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/jpg");
                    startActivityForResult(intent, REQUEST_GALLERY);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void uploadAudio() {
        final CharSequence[] options = { getString(R.string.upload_audio_record_audio), getString(R.string.upload_audio_choose_from_gallery), getString(R.string.upload_audio_cancel) };
        final AlertDialog.Builder builder = new AlertDialog.Builder(EditProductActivity.this);
        builder.setTitle(R.string.upload_audio_title);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (options[which].equals("Record audio")) {
                    record();
                } else if (options[which].equals("Choose from memory")) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("audio/*");
                    startActivityForResult(intent, REQUEST_AUDIO);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void record() {

        final DialogPlus dialogPlus = Master.dialogPlus(new ViewHolder(R.layout.audio_box), EditProductActivity.this);

        bPlay = (Button) dialogPlus.findViewById(R.id.bPlay);
        bRecord = (Button) dialogPlus.findViewById(R.id.bRecord);
        bStop = (Button) dialogPlus.findViewById(R.id.bStop);
        bUpload = (Button) dialogPlus.findViewById(R.id.bUpload);
        bCancel = (Button) dialogPlus.findViewById(R.id.bCancel);

        if (!hasMicrophone())
        {
            bStop.setEnabled(false);
            bPlay.setEnabled(false);
            bRecord.setEnabled(false);

            Toast.makeText(EditProductActivity.this,
                    R.string.label_toast_microphone_is_not_functioning_properly, Toast.LENGTH_LONG).show();
        }
        else {
            bPlay.setEnabled(false);
            bStop.setEnabled(false);

            tempAudio = "/myaudio" + randomno.nextInt(1000000) + ".mp3";
            recordedAudioFilePath = Environment.getExternalStorageDirectory() + tempAudio;

            TextView audio = (TextView) findViewById(R.id.tAudioLink);
            audio.setText(recordedAudioFilePath);

            bRecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        recordAudio(recordedAudioFilePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            bPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        playAudio(recordedAudioFilePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            bStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stopAudio();
                }
            });

            bUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isRecorded)
                    {
                        Toast.makeText(EditProductActivity.this, R.string.label_toast_please_record_an_audio_to_upload, Toast.LENGTH_SHORT).show();
                    }
                    else if(!Master.isNetworkAvailable(EditProductActivity.this))
                    {
                        Toast.makeText(EditProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        new UploadAudioTask(recordedAudioFilePath, dialogPlus).execute();
                    }
                    if(isPlaying)
                        stopAudio();
                }
            });

            bCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogPlus.dismiss();
                    isRecorded = false;
                    try {
                        //file1.delete();
                        file.delete();
                    } catch (Exception e) {

                    }
                }
            });
        }
    }


    public void recordAudio (String filepath) throws IOException
    {
        isRecording = true;
        bStop.setEnabled(true);
        bPlay.setEnabled(false);
        bRecord.setEnabled(false);
        bUpload.setEnabled(false);

        try {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setOutputFile(filepath);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mediaRecorder.start();

        mediaRecorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
            @Override
            public void onError(MediaRecorder mr, int what, int extra) {
                Toast.makeText(EditProductActivity.this, R.string.label_toast_the_recorder_couldnt_be_started_please_try_again, Toast.LENGTH_LONG).show();
            }
        });
        isRecorded = true;
    }

    public void stopAudio ()
    {
        isPlaying = false;
        bStop.setEnabled(false);
        bPlay.setEnabled(true);
        bUpload.setEnabled(true);
        if (isRecording)
        {
            bRecord.setEnabled(false);
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
            isRecording = false;
        }
        else
        {
            mediaPlayer.release();
            mediaPlayer = null;
            bRecord.setEnabled(true);
        }
    }

    public void playAudio (String filepath) throws IOException
    {
        isPlaying = true;
        bPlay.setEnabled(false);
        bRecord.setEnabled(false);
        bStop.setEnabled(true);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(filepath);
        mediaPlayer.prepare();
        mediaPlayer.start();

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                bPlay.setEnabled(true);
                bRecord.setEnabled(true);
                bStop.setEnabled(false);
            }
        });

        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(EditProductActivity.this, R.string.label_toast_the_player_couldnt_be_started_please_try_again
                        , Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }

    protected boolean hasMicrophone() {
        PackageManager pmanager = this.getPackageManager();
        return pmanager.hasSystemFeature(
                PackageManager.FEATURE_MICROPHONE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals(tempImage)) {
                        f = temp;
                        break;
                    }
                }

                try {

                    String filePath = f.getAbsolutePath();
                    //Uri fileUri = Uri.fromFile(f);

                    Intent intent = new Intent(getApplicationContext(),UploadImageDisplay.class);
                    intent.putExtra("imgpath",filePath);
                    intent.putExtra("productname",productName);
                    intent.putExtra("TAG", TAG);
                    startActivity(intent);



                    /*final Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    //bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                    bitmap = Master.readBitmap(Uri.fromFile(f), EditProductActivity.this, 2);
                    //bitmap = ImageUtils.decodeSampledBitmap(EditProductActivity.this, Uri.fromFile(f));

                    final String filePath = getResizedBitmap(bitmap,300);
                    File imageFile = new File(filePath);
                    final long size = imageFile.length()/1024;
                    final Uri uri = Uri.fromFile(new File(filePath));

                    final Bitmap compressedBitmap = Master.readBitmap(uri,EditProductActivity.this,2);

                    final DialogPlus dialog = Master.dialogPlus(new ViewHolder(R.layout.image_box), EditProductActivity.this);

                    //capturedImageFilePath = f.getAbsolutePath();

                    ivProduct = (ImageView) dialog.findViewById(R.id.ivProduct);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) dpWidth, (int) dpWidth);
                    ivProduct.setLayoutParams(layoutParams);
                    //ivProduct.setImageBitmap(bitmap);

                    //ivProduct.setImageBitmap(compressedBitmap);
                    rotateImage(filePath);

                    // Master.rotateImage(ivProduct, Master.getImageOrientation(f.getAbsolutePath())
                   //                     , ivProduct.getDrawable().getBounds().width()/2, ivProduct.getDrawable().getBounds().height()/2);

                    TextView imagePath = (TextView) findViewById(R.id.tImageLink);
                    //imagePath.setText(capturedImageFilePath);

                    imagePath.setText(filePath);


                    bSave = (Button) dialog.findViewById(R.id.bImageUpload);
                    bSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!Master.isNetworkAvailable(EditProductActivity.this)) {
                                Toast.makeText(EditProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_LONG).show();
                            } else {
                                //new UploadImageTask(capturedImageFilePath, dialog, bitmap,true).execute();

                                if(size<1025){
                                    //System.out.println("Normal Image path :  "+selectedImageFilePath+"   URI: "+selectedImageURI);
                                    //System.out.println("Compressed Image path :  "+filePath+"   URI: "+uri);
                                    //  Log.e("path" , filePath);
                                    // Log.e("path" , uri.toString());

                                    new UploadImageTask(filePath,dialog,compressedBitmap,false).execute();
                                }else{
                                    Toast.makeText(EditProductActivity.this,"Select an image which is less than 1Mb", Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    });

                    bCancel = (Button) dialog.findViewById(R.id.bCancel);
                    bCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            try {
                                file1.delete();
                                file.delete();
                                Master.clearBitmap(bitmap);
                            } catch (Exception e) {}
                        }
                    });*/

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            else if (requestCode == REQUEST_GALLERY)
            {
                Uri selectedImageURI = data.getData();
                if(selectedImageURI != null) {
                    try {
                        selectedImageFilePath = Master.getPath(EditProductActivity.this, selectedImageURI);

                        Intent intent = new Intent(getApplicationContext(),UploadImageDisplay.class);
                        intent.putExtra("imgpath",selectedImageFilePath);
                        intent.putExtra("productname",productName);
                        intent.putExtra("TAG", TAG);
                        startActivity(intent);

                        //Bitmap thumbnail = (BitmapFactory.decodeFile(selectedImageFilePath));

                        //Comment this part and test

                        /*final Bitmap bitmap = Master.readBitmap(selectedImageURI, EditProductActivity.this, 2);


                        *//*final String filePath = getResizedBitmap(bitmap,300);
                        File imageFile = new File(filePath);
                        final long size = imageFile.length()/1024;
                        final Uri uri = Uri.fromFile(new File(filePath));*//*

                        //final Bitmap compressedBitmap = Master.readBitmap(uri,EditProductActivity.this,2);


                        final DialogPlus dialog = Master.dialogPlus(new ViewHolder(R.layout.image_box), EditProductActivity.this);

                        ivProduct = (ImageView) dialog.findViewById(R.id.ivProduct);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) dpWidth, (int) dpWidth);
                        ivProduct.setLayoutParams(layoutParams);
                      //  ivProduct.setImageBitmap(bitmap);

                        //ivProduct.setImageBitmap(compressedBitmap);
                        //String fp = getAbsolutePath(selectedImageURI);
                        final Bitmap bits = rotateImage(selectedImageFilePath);

                        TextView imagePath = (TextView) findViewById(R.id.tImageLink);
                       // imagePath.setText(selectedImageFilePath);
                        imagePath.setText(selectedImageFilePath);

                        bSave = (Button) dialog.findViewById(R.id.bImageUpload);
                        bSave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!Master.isNetworkAvailable(EditProductActivity.this)) {
                                    Toast.makeText(EditProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_LONG).show();
                                } else {
                                   // new UploadImageTask(selectedImageFilePath, dialog, bitmap,false).execute();

                                    //new UploadImageTask(selectedImageFilePath,dialog,bitmap,false).execute();
                                    //Stuff to test rotation
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    bits.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                    byte[] byteArray = stream.toByteArray();

                                    Intent i = new Intent(getApplicationContext(), UploadImageDisplay.class);
                                    i.putExtra("img",byteArray);
                                    i.putExtra("imgPath","dcim");
                                    startActivity(i);

                                    *//*if(size<1025){
                                        //System.out.println("Normal Image path :  "+selectedImageFilePath+"   URI: "+selectedImageURI);
                                        //System.out.println("Compressed Image path :  "+filePath+"   URI: "+uri);
                                       // Log.e("path", filePath);
                                       // Log.e("path" , uri.toString());

                                        new UploadImageTask(filePath,dialog,compressedBitmap,false).execute();
                                    }else{
                                        Toast.makeText(EditProductActivity.this,"Select an image which is less than 1Mb", Toast.LENGTH_SHORT).show();
                                    }*//*
                                }

                            }
                        });

                        bCancel = (Button) dialog.findViewById(R.id.bCancel);
                        bCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    file1.delete();
                                    Master.clearBitmap(bitmap);
                                } catch (Exception e) {

                                }
                            }
                        });
                        ivProduct.setImageBitmap(bitmap);
                        ivProduct.setVisibility(View.VISIBLE);*/

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(EditProductActivity.this, R.string.label_toast_unable_to_get_the_file_path, Toast.LENGTH_LONG).show();
                    }
                }
            }
            else if(requestCode == REQUEST_AUDIO)
            {

                try {
                    selectedAudioFilePath = Master.getPath(EditProductActivity.this, data.getData());

                    final DialogPlus dialogPlus = Master.dialogPlus(new ViewHolder(R.layout.audio_box), EditProductActivity.this);
                    bPlay = (Button) dialogPlus.findViewById(R.id.bPlay);
                    bRecord = (Button) dialogPlus.findViewById(R.id.bRecord);
                    bStop = (Button) dialogPlus.findViewById(R.id.bStop);
                    bStop.setEnabled(false);
                    bUpload = (Button) dialogPlus.findViewById(R.id.bUpload);
                    bCancel = (Button) dialogPlus.findViewById(R.id.bCancel);

                    bRecord.setVisibility(View.GONE);

                    TextView audio = (TextView) dialogPlus.findViewById(R.id.tAudioLink);
                    audio.setText(selectedAudioFilePath);


                    File file = new File(selectedAudioFilePath);
                    double size = file.length();
                    size = size/(1024*1024.0);
                    MediaPlayer mp = new MediaPlayer();
                    FileInputStream fs;
                    FileDescriptor fd;
                    int length = 0;
                    try {
                        fs = new FileInputStream(file);
                        fd = fs.getFD();
                        mp.setDataSource(fd);
                        mp.prepare();
                        length = mp.getDuration();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mp.release();
                    String duration = String.format("%d min, %d sec",
                            TimeUnit.MILLISECONDS.toMinutes(length),
                            TimeUnit.MILLISECONDS.toSeconds(length) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(length))
                    );
                    Log.d("Length of audio", duration);
                    Log.d("Size of audio file", String.valueOf(size));
                    //Toast.makeText(getApplicationContext(), "Length of audio: "+duration+" :: "+ String.valueOf(size), Toast.LENGTH_LONG).show();



                    bPlay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                playAudio(selectedAudioFilePath);
                            } catch (IOException e) {
                                Toast.makeText(EditProductActivity.this, R.string.label_toast_cannot_play_audio, Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    bStop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            stopAudio();
                        }
                    });

                    bUpload.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!Master.isNetworkAvailable(EditProductActivity.this)) {
                                Toast.makeText(EditProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_LONG).show();
                            } else {
                                //for calculating audio length

                                new UploadAudioTask(selectedAudioFilePath, dialogPlus).execute();
                            }
                            if (isPlaying)
                                stopAudio();
                        }
                    });

                    bCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogPlus.dismiss();
                        }
                    });
                }
                catch (Exception e)
                {
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_unable_to_get_the_file_path, Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    public String getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        String filepath = "";
        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        FileOutputStream out = null;
        try {

            String path = Environment.getExternalStorageDirectory().toString();

            long time= System.currentTimeMillis();
            File file = new File(path, AdminDetails.getMobileNumber()+"-"+time+".png");
            //System.out.println("logo path : "+path);
            filepath = file.getAbsolutePath();
            if (file.exists()){
                file.delete();
            }
            out = new FileOutputStream(file);
            Bitmap bitmp = Bitmap.createScaledBitmap(image, width, height, true);
            bitmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return filepath;
    }




    /*public String getImagePath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    public String getAudioPath(Uri uri)
    {
        String[] projection = { MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }*/
/*
    public void requestStoragePermission()
    {
        if (Build.VERSION.SDK_INT >= 23){
        // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(EditProductActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(EditProductActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(EditProductActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                    // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        }
    }
*/

    private void save()
    {
        String productName, productId,productPrice, productQuantity,productDescription;
        /*productName = productList.get(position);
        productPrice = priceList.get(position);
        productQuantity = quantityList.get(position);*/

        if(fromWhere != null && fromWhere.equals("fromDashboardActivity")){
        productName = this.productName ;
            productId=this.productId;

       /* productPrice = this.productUnitPrice.toString();
        productQuantity = this.stockQuantity+"";
        productDescription=this.productDescription;*/

       }else{

            productId = Master.productIdList.get(position);

            productName = Master.productList.get(position);
           // System.out.println("idddd--------"+productId);
       /* productPrice = Master.priceList.get(position);
        productQuantity = Master.quantityList.get(position);
        productDescription = Master.descList.get(position);*/
       }

        String enteredName, enteredPrice, enteredQuantity,enteredDescription;
        enteredName = eProductName.getText().toString().trim();
        enteredPrice = eProductPrice.getText().toString().trim();
        enteredQuantity = eProductQuantity.getText().toString().trim();
        enteredDescription=eProductDescription.getText().toString().trim();

        if(enteredName.equals(""))
        {
            Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_a_product_name, Toast.LENGTH_SHORT).show();
        }
        else if(enteredPrice.equals(""))
        {
            Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_price,Toast.LENGTH_SHORT).show();
        }
        else if(enteredQuantity.equals(""))
        {
            Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_quantity,Toast.LENGTH_SHORT).show();
        }
        else
        {



            if (!enteredPrice.equals("")) {

                try {

                    double price = Double.parseDouble(enteredPrice);
                    if (price == 0.0) {
                        Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_price_greater_than_zero, Toast.LENGTH_SHORT).show();

                    }
                    else
                    {

                        boolean flag = false;

                        //Round upto to 2 decimal places
                      //  double a = Double.parseDouble(enteredPrice);
                        DecimalFormat decimalFormat=new DecimalFormat("#.00");
                        enteredPrice=decimalFormat.format(price);

                        for(int i = 0; i < recyclerViewIndex; ++i)
                        {
                            if(i == position) // Skip checking the same product
                                continue;
                /*if(productList.get(i).equals(enteredName))
                {
                    flag = true;
                    break;
                }*/
                            if(Master.productList.get(i).equals(enteredName))
                            {
                                flag = true;
                                break;
                            }
                        }
                        if(!flag)
                        {
                            JSONObject jsonObject = new JSONObject();
                            try
                            {
                                int id = Integer.parseInt(productId);

                                jsonObject.put("id",id);
                                jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                jsonObject.put("name", productName);
                                jsonObject.put("newname", enteredName);
                                jsonObject.put("qty", enteredQuantity);
                                jsonObject.put("rate", enteredPrice);
                                jsonObject.put("newdesc",enteredDescription);
                            }
                            catch(JSONException e)
                            {
                                e.printStackTrace();
                                //Toast.makeText(EditProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                                Toast.makeText(EditProductActivity.this, R.string.label_toast_data_send_failed, Toast.LENGTH_SHORT).show();
                                Log.d("EXCEPTION","JSON EXCEPTION");
                            }
                            if(Master.isNetworkAvailable(EditProductActivity.this))
                            {

                                //System.out.println("edit product json----------"+jsonObject);

                                 new EditProductTask(EditProductActivity.this, position, enteredName, enteredPrice, enteredQuantity,enteredDescription,productId).execute(jsonObject);
                            }
                            else
                            {
                                Toast.makeText(EditProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_unique_product, Toast.LENGTH_SHORT).show();
                        }



                    }
                } catch (Exception e) {
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_valid_price, Toast.LENGTH_SHORT).show();

                }
            }

        }
    }

    //---------------------EditProduct Asynctask---------------------------------------------------------
    /******************************************************************
     API:
     /api/product/edit

     body:
     {
     "orgabbr":"Test2",
     "name":"Pumpkin",
     "newname":"Pumpkins",
     "qty":"120",
     "rate":"125"
     }

     response:
     {
     "edit": "success"
     }
     *******************************************************************/

    //-------------------------------------for editing product-----------------------------------
    public class EditProductTask extends AsyncTask<JSONObject, String, String>
    {
        Context context;
        String response, productName, productPrice, productQuantity,productDescription,productId;
        ProgressDialog pd;
        int position;

        EditProductTask(Context context, int position, String productName, String productPrice, String productQuantity ,String productDescription,String productId )
        {
            this.context = context;
            this.productName = productName;
            this.productPrice = productPrice;
            this.productQuantity = productQuantity;
            this.productDescription=productDescription;
            this.position = position;
            this.productId=productId;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            //Log.d("JSON Input", String.valueOf(params[0]));
            response = getJSON.getJSONFromUrl(Master.getEditProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                JSONObject jsonObject = new JSONObject(message);
                if(jsonObject.getString("edit").toString().equals("success"))
                {
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_Product_updated_successfully, Toast.LENGTH_SHORT).show();
                    if(fromWhere==null) {
                        Master.productList.set(position, productName);
                        Master.priceList.set(position, productPrice);
                        Master.quantityList.set(position, productQuantity);
                        Master.descList.set(position, productDescription);
                    }

                     updateProductSearchList(productId,productName,productDescription,productPrice,productQuantity);
                }
                else
                {
                    Master.alertDialog(EditProductActivity.this, getString(R.string.label_toast_something_went_worng), getString(R.string.label_alertdialog_ok));
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(EditProductActivity.this, R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
            }

        }
    }



    int updateProductSearchList(String id,String name,String description,String price,String quantity)
    {

       // System.out.println("id---------"+id);
        for(int i = 0; i < Master.productTypeSearchList.size(); ++i)
        {
            for(int j = 0; j < Master.productTypeSearchList.get(i).productItems.size(); ++j)
            {
                if(Master.productTypeSearchList.get(i).productItems.get(j).getID().equals(id))
                {
                   // System.out.println("name---"+name+" price-----"+price);
                    Master.productTypeSearchList.get(i).productItems.get(j).setName(name);
                    Master.productTypeSearchList.get(i).productItems.get(j).setDescription(description);
                    Master.productTypeSearchList.get(i).productItems.get(j).setUnitPrice(Double.parseDouble(price));
                    Master.productTypeSearchList.get(i).productItems.get(j).setStockQuantity(Integer.parseInt(quantity));

                    //setTitle(Master.productTypeSearchList.get(i).getName());
                    return j;
                }
            }
        }
        return -1;
    }




    //---------------------End of EditProduct Asynctask-------------------------------------------------

    //------------------------------- Asynctask to upload Image ----------------------------------------------
    /*class UploadImageTask extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        String path;
        DialogPlus dialog;
        Bitmap bitmap;
        Boolean isCaptured;
        String imagePath;

        UploadImageTask(String path, DialogPlus dialogPlus, Bitmap bitmap,Boolean isCaptured)
        {
            this.bitmap = bitmap;
            this.path = path;
            dialog = dialogPlus;
            this.isCaptured=isCaptured;
        }
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(EditProductActivity.this);
            pd.setMessage(getString(R.string.pd_uploading_file));
            pd.setCancelable(false);
            pd.show();

           // System.out.println(path);

            file = new File(path);

            //System.out.println(file.getAbsolutePath());
           // System.out.println(file.getParent());

            imagePath = file.getParent() + "/" + "myImage" + randomno.nextInt(1000000) + ".jpg";


            file1 = new File(imagePath);
            try
            {
                Master.copyFile(file, file1);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String response;
            response = Master.okhttpUpload(file1, Master.getUploadImageURL(AdminDetails.getAbbr(), productName),
                    Master.IMAGE_FILE_TYPE, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                JSONObject responseObject = new JSONObject(message);
                message=responseObject.optString("response");
                //System.out.println("message"+message.toString());

                if(message.equals("Image upload successful"))
                {
                    Toast.makeText(EditProductActivity.this,
                            R.string.label_toast_image_has_been_uploaded_successfully, Toast.LENGTH_LONG).show();
                    Master.clearBitmap(bitmap);
                    dialog.dismiss();
                }
                else
                {
                    Toast.makeText(EditProductActivity.this,
                            R.string.label_cannot_connect_to_the_server, Toast.LENGTH_LONG).show();

                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(EditProductActivity.this,
                        R.string.label_toast_something_went_worng, Toast.LENGTH_LONG).show();
            }
        }
    }*/
//---------------------End of Upload Image Asynctask-------------------------------------------------


    //------------------------------- Asynctask to upload Audio ----------------------------------------------
    class UploadAudioTask extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        String path;
        DialogPlus dialog;

        UploadAudioTask(String path, DialogPlus dialogPlus)
        {
            this.path = path;
            dialog = dialogPlus;
        }
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(EditProductActivity.this);
            pd.setMessage(getString(R.string.pd_uploading_file));
            pd.setCancelable(false);
            pd.show();

            file = new File(path);

            /*String audioPath = file.getParent() +  "/myAudio" + randomno.nextInt(1000000) + ".mp3";
            file1 = new File(audioPath);
            try
            {
                Master.copyFile(file, file1);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }*/
        }

        @Override
        protected String doInBackground(Void... params) {
            String response;
            response = Master.okhttpUpload(file, Master.getUploadAudioURL(AdminDetails.getAbbr(), productName),
                    Master.IMAGE_FILE_TYPE, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            if(pd != null && pd.isShowing())
             pd.dismiss();
            try
            {
                JSONObject responseObject = new JSONObject(s);
                if(responseObject.get("response").toString().equals("Audio upload successful"))
                {
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_audio_has_been_uploaded_successfully, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
                else if(responseObject.get("response").toString().equals("timeout")){
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_timeout_upload, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
                else
                {
/*
                    Toast.makeText(EditProductActivity.this,
                            R.string.label_toast_something_went_worng, Toast.LENGTH_LONG).show();
*/
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_audio_upload_failed, Toast.LENGTH_SHORT).show();
                    Log.d("Audio not uploaded: ", "No proper response");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(EditProductActivity.this,
                        R.string.label_toast_something_went_worng, Toast.LENGTH_LONG).show();
                Log.d("Audio not uploaded: ", "EXCEPTION");

            }
        }
    }
//---------------------End of Upload Audio Asynctask-------------------------------------------------
}
