package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by SidRama on 15/01/16.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class MyGcmListenerService extends GcmListenerService {

    static String ACTION = "update";
    private static final String TAG = "MyGcmListenerService";
    /**
     * Called when message is received.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        List<NotificationBundle> notifsBundle = new ArrayList<NotificationBundle>();
        String message = data.getString(("message"));
        String title = data.getString(("title"));
        int id = Integer.parseInt(data.getString("id"));
        //System.out.println(message + " " + title + " " + id);
        Type listOfTestObject = new TypeToken<List<NotificationBundle>>() {
        }.getType();
        Gson gson = new Gson();
        if (id == 0) {
          //  System.out.println("------------Notification should come------------------");
            sendNotification(message, title);

/*
            if (!prefs.contains("orderNotifs")) {
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String s = gson.toJson(notifsBundle, listOfTestObject);
                editor.putString("orderNotifs", s);
                editor.commit();
                sendNotification(message, title);
                System.out.println("I m in 1");

            } else {

                String s = prefs.getString("orderNotifs", "");
                notifsBundle = gson.fromJson(s, listOfTestObject);
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String fin = gson.toJson(notifsBundle, listOfTestObject);
                editor.putString("orderNotifs", fin);
                editor.commit();
                sendNotificationWithStyle(message, title, notifsBundle);
                System.out.println("I m in 2");
            }*/
        }
        else if (id == 1) {
           // int userId  = Integer.parseInt(data.getString("userId"));

            //sendMemberNotification(message, title, userId);

            if( !prefs.contains("memberNotifs")) {
                int userId  = Integer.parseInt(data.getString("userId"));
                NotificationBundle bundle = new NotificationBundle(message, title, userId);
                notifsBundle.add(bundle);
                String s = gson.toJson(notifsBundle, listOfTestObject);
                editor.putString("memberNotifs",s);
                editor.commit();
                sendMemberNotification(message, title, userId);
                System.out.println("I m in 3");
            }
            else {
                int userId  = Integer.parseInt(data.getString("userId"));
                String s = prefs.getString("memberNotifs", "");
                notifsBundle = gson.fromJson(s, listOfTestObject);
                NotificationBundle bundle = new NotificationBundle(message, title, userId);
                notifsBundle.add(bundle);
                String fin = gson.toJson(notifsBundle, listOfTestObject);
                editor.putString("memberNotifs", fin);
                editor.commit();
                sendMemberNotificationWithStyle(message, title, notifsBundle);
                System.out.println("I m in 4");
            }
        }
        else if(id == 2)
        {
            Intent updateComplete = new Intent(ACTION);
            updateComplete.putExtra("saved", data.getString("saved"));
            updateComplete.putExtra("processed", data.getString("processed"));
            updateComplete.putExtra("cancelled", data.getString("cancelled"));
            updateComplete.putExtra("totalUsers", data.getString("totalUsers"));
            updateComplete.putExtra("newUsersToday", data.getString("newUsersToday"));
            updateComplete.putExtra("pendingUsers", data.getString("pendingUsers"));
            updateComplete.putExtra("paid",data.getString("paid"));
            updateComplete.putExtra("unpaid",data.getString("unpaid"));
            updateComplete.putExtra("delivered",data.getString("delivered"));
            LocalBroadcastManager.getInstance(this).sendBroadcast(updateComplete);
           /* System.out.println(data.getString("saved") + " " +
                    data.getString("processed") + " " +
                    data.getString("cancelled") + " " +
                    data.getString("totalUsers") + " " +
                    data.getString("newUsersToday") + " " +
                    data.getString("pendingUsers") + " " +
                    data.getString("paid") + " " +
                    data.getString("unpaid") + " " +
                    data.getString("delivered") );*/
          //  System.out.println("I m in 5");
        }
    }

    private void sendMemberNotification(String message, String title, int userId) {

        Intent intent = new Intent(this, DashboardActivity.class);
         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1/* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_face_white_36dp)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.ic_done_black_24dp,"Approve",getPendingAction(this,userId,0))
                .addAction(R.drawable.ic_clear_black_24dp,"Ignore",getPendingAction(this,userId,1));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1 /* ID of notification */, notificationBuilder.build());


    }

    public PendingIntent getPendingAction(Context context, int id, int code) {
        Intent intent = new Intent(context, NotificationBroadcastReceiver.class);
        intent.putExtra("id",id);
        intent.putExtra("code",code);
        final String ACTION = "admin.lokacart.broadcast";
        intent.setAction(ACTION);
        Random generator = new Random();
        return PendingIntent.getBroadcast(context, generator.nextInt(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    public void sendMemberNotificationWithStyle(String message, String title, List<NotificationBundle> notifsBundle) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1 /*Request code*/ , intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_face_white_36dp)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.ic_done_all_black_24dp, "Approve All",getPendingAction(this,0,2))
                .addAction(R.drawable.ic_clear_black_24dp, "Ignore All", getPendingAction(this,0,1));


        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(notifsBundle.size()+" new member requests:");
        Iterator <NotificationBundle> iter = notifsBundle.iterator();
        while(iter.hasNext()) {
            NotificationBundle notif = iter.next();
            inboxStyle.addLine(notif.getText());

        }
        notificationBuilder.setStyle(inboxStyle);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1  /*ID of notification*/ , notificationBuilder.build());


    }
    private void sendNotification(String message, String title) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_shopping_cart_white_36dp)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }



   /* private void sendNotificationWithStyle(String message, String title, List<NotificationBundle> notifsBundle) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_shopping_cart_white_36dp)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(notifsBundle.size()+" new orders received:");
        Iterator <NotificationBundle> iter = notifsBundle.iterator();
        while(iter.hasNext()) {
            NotificationBundle notif = iter.next();
            inboxStyle.addLine(notif.getText());
        }
        notificationBuilder.setStyle(inboxStyle);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());
    }*/
    class AsyncT extends AsyncTask<JSONObject, Void, Void> {

        Context context;
        AsyncT(Context context)
        {
            this.context = context;

        }
        protected Void doInBackground(JSONObject... params) {
            GetJSON getJson = new GetJSON();
            String response = getJson.getJSONFromUrl(Master.serverURL+"/app/deregistertoken",params[0],"POST",false,null,null);
            return null;

        }
        protected void onPostExecute(String response) {
        }
    }




}