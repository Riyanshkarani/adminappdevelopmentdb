package admin.lokacart.ict.mobile.com.adminapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import admin.lokacart.ict.mobile.com.adminapp.adapters.ProductRecyclerViewAdapter;

public class ProductActivity extends AppCompatActivity implements MyListener{

    Dialog dialog;

    public RecyclerView mRecyclerView;
    public RecyclerView.Adapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;
    public int activity_position;
    public int onResumePosition;

    DashboardActivity dashboardActivity;
    //ArrayList<String> productList, priceList, quantityList;
    JSONObject responseObject;
    public static int recyclerViewIndex;

    String LOG_TAG, productType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Master.getAdminData(getApplicationContext());
        LOG_TAG = "Product Activity";
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(LOG_TAG);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                addProduct();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        productType = getIntent().getStringExtra("product type");
        activity_position = getIntent().getIntExtra("position",-1);


        setTitle(productType);

        mRecyclerView = (RecyclerView) findViewById(R.id.productRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ProductActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        /*productList = new ArrayList<>();
        quantityList = new ArrayList<>();
        priceList = new ArrayList<>();*/

        Master.productList = new ArrayList<>();
        Master.quantityList = new ArrayList<>();
        Master.priceList = new ArrayList<>();
        Master.descList = new ArrayList<>();
        Master.productIdList = new ArrayList<>();
        Master.productStatusList = new ArrayList<>();

        recyclerViewIndex = 0;

        new GetProductDetailsTask(productType).execute();

        dashboardActivity = new DashboardActivity();

    }

    @Override
    protected void onResume() {
        super.onResume();
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity("ProductActivity");
        Master.getAdminData(getApplicationContext());

    }

    @Override
    public void onCardClickListener(int position, int cat, Object obj) {
        if(cat == Master.productClickKey)
        {
            //Log.e("Vish", productList.get(position) + " clicked");
            //modifyProduct(position);
            Intent i = new Intent(ProductActivity.this, EditProductActivity.class);
            /*i.putStringArrayListExtra("productList", productList);
            i.putStringArrayListExtra("priceList", priceList);
            i.putStringArrayListExtra("quantityList", quantityList);*/
            i.putExtra("position", position);
            i.putExtra("size", recyclerViewIndex);
            onResumePosition = position;
            startActivityForResult(i, 0);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Master.getAdminData(getApplicationContext());
        if(mAdapter != null){
            mAdapter.notifyItemChanged(onResumePosition);
        }
    }

    @Override
    public void onCardLongClickListener(int position, int category, Object obj) {
        if(category == Master.productClickKey)
        {
            //Log.e("Vish", productList.get(position) + " Long clicked");
            modifyProduct(position);
        }
    }

    /********************************A class to fetch products from the server.********************/
    public class GetProductDetailsTask extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        String response, productType;

        public GetProductDetailsTask(String productType) {
            this.productType = productType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ProductActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String getProductURL = Master.getProductURL() + AdminDetails.getAbbr();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(getProductURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if(pd != null && pd.isShowing())
                pd.dismiss();

            if (response.equals("exception"))
            {
                Master.alertDialog(ProductActivity.this, getString(R.string.label_cannot_connect_to_the_server),getString(R.string.label_alertdialog_ok));
            }
            else
            {
                try
                {
                    JSONObject jsonObject;
                    responseObject = new JSONObject(response);
                    JSONArray jsonArray = responseObject.getJSONArray(productType);
                    if(jsonArray.length() > 0)
                    {
                        for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                        {
                            jsonObject = (JSONObject) jsonArray.get(recyclerViewIndex);
                            /*productList.add(recyclerViewIndex, jsonObject.getString("name"));
                            priceList.add(recyclerViewIndex, jsonObject.getString("unitRate"));
                            quantityList.add(recyclerViewIndex, jsonObject.getString("quantity"));*/

                            Master.productList.add(recyclerViewIndex, jsonObject.getString("name"));
                            Master.priceList.add(recyclerViewIndex, jsonObject.getString("unitRate"));
                            Master.quantityList.add(recyclerViewIndex, jsonObject.getString("quantity"));
                            Master.productIdList.add(recyclerViewIndex,jsonObject.getString("id"));
                            Master.productStatusList.add(recyclerViewIndex, jsonObject.getString("status"));

                            if(jsonObject.getString("description").equals("null")){
                                Master.descList.add(recyclerViewIndex,"");
                            }
                            else {
                                Master.descList.add(recyclerViewIndex, jsonObject.getString("description"));
                            }

                        }

                        //mAdapter = new ProductRecyclerViewAdapter(productList, quantityList, priceList, ProductActivity.this);
                        mAdapter = new ProductRecyclerViewAdapter(Master.productList, Master.quantityList, Master.priceList, Master.productStatusList, ProductActivity.this);
                        mRecyclerView.setAdapter(mAdapter);
                        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ProductActivity.this, mRecyclerView, Master.productClickKey, null));
                    }
                    else
                    {
                        alertDialog();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(ProductActivity.this, R.string.label_toast_data_send_failed, Toast.LENGTH_SHORT).show();
                    Log.d("EXCEPTION","JSON EXCEPTION");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        getSupportFragmentManager().popBackStack();
        return true;
    }

    /*********************************End of class*************************************************/


    public void alertDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProductActivity.this);

        builder.setMessage(R.string.label_there_are_no_products_in_this_product_type);
        builder.setCancelable(false);

        builder.setPositiveButton(R.string.builder_add,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addProduct();
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(R.string.builder_back,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert11 = builder.create();
        alert11.show();
    }
    private void addProduct() {

        dialog = new Dialog(ProductActivity.this);
        dialog.setContentView(R.layout.product_box);
        dialog.setTitle(R.string.dialog_title_add_product);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eProductName, eProductPrice, eProductQuantity,eProductDescription;
        final TextView tProductNameCharsRemaining,tDescCharRemaining;
        final Button bProductConfirm, bProductCancel, bProductDelete, bProductEdit;

        eProductName = (EditText) dialog.findViewById(R.id.eProductName);
        eProductPrice = (EditText) dialog.findViewById(R.id.eProductPrice);
        eProductQuantity = (EditText) dialog.findViewById(R.id.eProductQuantity);
        eProductDescription=(EditText) dialog.findViewById(R.id.eProductDescription);
        tProductNameCharsRemaining=(TextView) dialog.findViewById(R.id.tProductNameChars);
        tDescCharRemaining = (TextView)dialog.findViewById(R.id.tDescCharRemaining);

        bProductEdit = (Button) dialog.findViewById(R.id.bProductEdit);
        bProductEdit.setVisibility(View.GONE);
        bProductDelete = (Button) dialog.findViewById(R.id.bProductDelete);
        bProductDelete.setVisibility(View.GONE);
        bProductConfirm = (Button) dialog.findViewById(R.id.bProductConfirm);
        bProductCancel = (Button) dialog.findViewById(R.id.bProductCancel);



        eProductName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != -1)
                    tProductNameCharsRemaining.setText((50 - s.length()) + " " +  getString(R.string.textview_characters_left));
            }
        });

        eProductDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != -1)
                    tDescCharRemaining.setText((500 - s.length()) + " " + getString(R.string.textview_characters_left));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        bProductCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bProductConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO send the new product name
                String newProductName, newProductPrice, newProductQuantity,newDesciption;
                newProductName = eProductName.getText().toString().trim();
                newProductPrice = eProductPrice.getText().toString().trim();
                newProductQuantity = eProductQuantity.getText().toString().trim();
                newDesciption=eProductDescription.getText().toString().trim();

                if (newProductName.equals("")) {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_a_product_name, Toast.LENGTH_SHORT).show();
                } else if (newProductQuantity.equals("")) {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_quantity, Toast.LENGTH_SHORT).show();
                } else if (newProductPrice.equals("")) {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_price, Toast.LENGTH_SHORT).show();
                }
                else if (!Master.isNetworkAvailable(ProductActivity.this)) {
                    Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                } else {


                    if (!newProductPrice.equals("")) {

                        try {

                            double price = Double.parseDouble(newProductPrice);
                            if (price == 0.0) {
                                Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_price_greater_than_zero, Toast.LENGTH_SHORT).show();

                            }
                            else
                            {


                                boolean flag = true;
                                for (int i = 0; i < recyclerViewIndex; ++i) {
                        /*if (productList.get(i).equals(newProductName)) {
                            flag = false;
                            break;
                        }*/
                                    if (Master.productList.get(i).equals(newProductName)) {
                                        flag = false;
                                        break;
                                    }
                                }
                                if (flag) {
                                    JSONObject jsonObject = new JSONObject();
                                    try {
                                        jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                        jsonObject.put("name", newProductName);
                                        jsonObject.put("productType", productType);
                                        jsonObject.put("rate", newProductPrice);
                                        jsonObject.put("qty", newProductQuantity);
                                        jsonObject.put("description", newDesciption);

                                      //  System.out.println("product json----------"+jsonObject.toString());
                                    } catch (JSONException e) {
                                    }
                                     new AddProductTask(ProductActivity.this, newProductName, newProductPrice, newProductQuantity,newDesciption).execute(jsonObject);
                                }
                                else
                                {
                                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_unique_product, Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (Exception e) {
                            Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_valid_price, Toast.LENGTH_SHORT).show();

                        }
                    }


                }

            }
        });
    }
// --------------------End of addProduct function---------------------------------------------------


//---------------------AddProduct Asynctask---------------------------------------------------------

    public void modifyProduct(final int position)
    {
        final String productName, productPrice, productQuantity,productDescription, productStatus, productId;
        /*productName = productList.get(position);
        productPrice = priceList.get(position);
        productQuantity = quantityList.get(position);*/

        productName = Master.productList.get(position);
        productPrice = Master.priceList.get(position);
        productQuantity = Master.quantityList.get(position);
        productDescription=Master.descList.get(position);
        productStatus = Master.productStatusList.get(position);
        productId = Master.productIdList.get(position);

        dialog = new Dialog(ProductActivity.this);
        dialog.setContentView(R.layout.product_box);
        dialog.setTitle(getString(R.string.dialog_title_modify) + " " + productName);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eProductName, eProductPrice, eProductQuantity,eProductDescription;
        final Button bProductConfirm, bProductCancel, bProductDelete, bProductEdit;
        final TextView tProductNameCharRemaining,tDescCharRemaining,tPriceCharRemaining,tStockCharRemaining;

        final LinearLayout linearLayout;

        linearLayout=(LinearLayout)dialog.findViewById(R.id.leditProduct);

        linearLayout.setMinimumHeight(30);

        eProductName = (EditText) dialog.findViewById(R.id.eProductName);
        eProductName.setVisibility(View.GONE);

        eProductPrice = (EditText) dialog.findViewById(R.id.eProductPrice);
        eProductPrice.setVisibility(View.GONE);

        eProductQuantity = (EditText) dialog.findViewById(R.id.eProductQuantity);
        eProductQuantity.setVisibility(View.GONE);

        eProductDescription=(EditText) dialog.findViewById(R.id.eProductDescription);
        eProductDescription.setVisibility(View.GONE);

        tProductNameCharRemaining=(TextView) dialog.findViewById(R.id.tProductNameChars);
        tProductNameCharRemaining.setVisibility(View.GONE);

        tDescCharRemaining = (TextView)dialog.findViewById(R.id.tDescCharRemaining);
        tDescCharRemaining.setVisibility(View.GONE);

        tPriceCharRemaining =(TextView)dialog.findViewById(R.id.tPriceCharRemaining);
        tPriceCharRemaining.setVisibility(View.GONE);

        tStockCharRemaining = (TextView) dialog.findViewById(R.id.tStockCharRemaining);
        tStockCharRemaining.setVisibility(View.GONE);


        bProductEdit = (Button) dialog.findViewById(R.id.bProductEdit);

        bProductDelete = (Button) dialog.findViewById(R.id.bProductDelete);
        if(productStatus.equals("0")){
            bProductDelete.setText(getString(R.string.label_button_enable));
        }
        else{
            bProductDelete.setText(getString(R.string.label_button_disable));
        }

        bProductConfirm = (Button) dialog.findViewById(R.id.bProductConfirm);
        bProductConfirm.setVisibility(View.GONE);

        bProductCancel = (Button) dialog.findViewById(R.id.bProductCancel);

        bProductCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bProductEdit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                eProductName.setVisibility(View.VISIBLE);
                eProductName.setText(productName);
                eProductPrice.setVisibility(View.VISIBLE);
                eProductPrice.setText(productPrice);
                eProductQuantity.setVisibility(View.VISIBLE);
                eProductQuantity.setText(productQuantity);
                eProductDescription.setVisibility(View.VISIBLE);
                eProductDescription.setText(productDescription);
                tProductNameCharRemaining.setVisibility(View.VISIBLE);
                tDescCharRemaining.setVisibility(View.VISIBLE);


                tDescCharRemaining.setText((500 - eProductDescription.length()) + " " + getString(R.string.textview_characters_left));

                tProductNameCharRemaining.setText((50 - eProductName.length()) + " " + getString(R.string.textview_characters_left));

                eProductName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() != -1)
                            tProductNameCharRemaining.setText((50 - s.length()) + " " + getString(R.string.textview_characters_left));
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                eProductDescription.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(s.length() != -1)
                            tDescCharRemaining.setText((500 - s.length()) + " " + getString(R.string.textview_characters_left));
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                bProductEdit.setVisibility(View.GONE);
                bProductDelete.setVisibility(View.GONE);
                bProductConfirm.setVisibility(View.VISIBLE);
            }
        });

        bProductConfirm.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                String enteredName, enteredPrice, enteredQuantity,enteredDescription;
                enteredName = eProductName.getText().toString().trim();
                enteredPrice = eProductPrice.getText().toString().trim();
                enteredQuantity = eProductQuantity.getText().toString().trim();
                enteredDescription=eProductDescription.getText().toString().trim();

                if(enteredName.equals(""))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_a_product_name,Toast.LENGTH_SHORT).show();
                }
                else if(enteredPrice.equals(""))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_price,Toast.LENGTH_SHORT).show();
                }
                else if(enteredQuantity.equals(""))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_quantity,Toast.LENGTH_SHORT).show();
                }
                else
                {



                    if (!enteredPrice.equals("")) {

                        try {

                            double price = Double.parseDouble(enteredPrice);
                            if (price == 0.0) {
                                Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_price_greater_than_zero, Toast.LENGTH_SHORT).show();

                            }
                            else
                            {


                                boolean flag = false;

                                //Round upto to 2 decimal places
                                //  double a = Double.parseDouble(enteredPrice);
                                DecimalFormat decimalFormat=new DecimalFormat("#.00");
                                enteredPrice=decimalFormat.format(price);

                                for(int i = 0; i < recyclerViewIndex; ++i)
                                {
                                    if(i == position) // Skip checking the same product
                                        continue;
                /*if(productList.get(i).equals(enteredName))
                {
                    flag = true;
                    break;
                }*/
                                    if(Master.productList.get(i).equals(enteredName))
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if(!flag)
                                {
                                    JSONObject jsonObject = new JSONObject();
                                    try
                                    {
                                        int id = Integer.parseInt(productId);

                                        jsonObject.put("id",id);
                                        jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                        jsonObject.put("name", productName);
                                        jsonObject.put("newname", enteredName);
                                        jsonObject.put("qty", enteredQuantity);
                                        jsonObject.put("rate", enteredPrice);
                                        jsonObject.put("newdesc",enteredDescription);
                                    }
                                    catch(JSONException e)
                                    {
                                        e.printStackTrace();
                                        //Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                                        Toast.makeText(ProductActivity.this, R.string.label_toast_data_send_failed, Toast.LENGTH_SHORT).show();
                                        Log.d("EXCEPTION","JSON EXCEPTION");
                                    }
                                    if(Master.isNetworkAvailable(ProductActivity.this))
                                    {

                                       // System.out.println("edit dialog product json----------"+jsonObject);

                                        new EditProductTask(ProductActivity.this, position, enteredName, enteredPrice, enteredQuantity,enteredDescription).execute(jsonObject);
                                        dialog.dismiss();

                                    }
                                    else
                                    {
                                        Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else
                                {
                                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_unique_product, Toast.LENGTH_SHORT).show();
                                }



                            }
                        } catch (Exception e) {
                            Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_valid_price, Toast.LENGTH_SHORT).show();

                        }
                    }



/*
                    //------------------------------------------------------------------------------------------------


                    //Round upto to 2 decimal places
                    double a = Double.parseDouble(enteredPrice);
                    DecimalFormat decimalFormat=new DecimalFormat("#.00");
                    enteredPrice=decimalFormat.format(a);

                    boolean flag = false;
                    for(int i = 0; i < recyclerViewIndex; ++i)
                    {
                        if(i == position) // Skip checking the same product
                            continue;
                        *//*if(productList.get(i).equals(enteredName))
                        {
                            flag = true;
                            break;
                        }*//*
                        if(Master.productList.get(i).equals(enteredName))
                        {
                            flag = true;
                            break;
                        }
                    }
                    if(!flag)
                    {
                        JSONObject jsonObject = new JSONObject();
                        try
                        {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("name", productName);
                            jsonObject.put("newname", enteredName);
                            jsonObject.put("qty", enteredQuantity);
                            jsonObject.put("rate", enteredPrice);
                            jsonObject.put("newdesc",enteredDescription);
                        }
                        catch(JSONException e)
                        {
                            Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }
                        if(Master.isNetworkAvailable(ProductActivity.this))
                        {
                            new EditProductTask(ProductActivity.this, position, enteredName, enteredPrice, enteredQuantity,enteredDescription).execute(jsonObject);
                            dialog.dismiss();
                        }
                        else
                        {
                            Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_unique_product, Toast.LENGTH_SHORT).show();
                    }*/
                }
            }
        });

        bProductDelete.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v)
            {
                JSONObject jsonObject = new JSONObject();
                try {
                   /* jsonObject.put("orgabbr", AdminDetails.getAbbr());
                    jsonObject.put("name", productName);*/
                    int id = Integer.parseInt(productId);

                    jsonObject.put("id",id);
                    if(productStatus.equals("0")) {
                        jsonObject.put("status",1);
                    }
                    else{
                        jsonObject.put("status",0);
                    }
                  //  System.out.println(jsonObject.toString());
                }
                catch (JSONException e)
                {
                }
                if(Master.isNetworkAvailable(ProductActivity.this))
                {
                   // new DeleteProductTask(ProductActivity.this, productName, position).execute(jsonObject);
                    new DisableEnableProductTask(ProductActivity.this, productId, position, productStatus).execute(jsonObject);
                    dialog.dismiss();
                }
                else
                {
                    Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }


//---------------------EditProduct Asynctask---------------------------------------------------------
    /******************************************************************
    API:
        /api/product/edit

        body:
        {
          "orgabbr":"Test2",
          "name":"Pumpkin",
           "newname":"Pumpkins",
           "qty":"120",
            "rate":"125"
        }

        response:
        {
            "edit": "success"
        }
     *******************************************************************/
    public class EditProductTask extends AsyncTask<JSONObject, String, String>
    {
        Context context;
        String response, productName, productPrice, productQuantity,productDescription;
        ProgressDialog pd;
        int position;

        EditProductTask(Context context, int position, String productName, String productPrice, String productQuantity,String productDescription)
        {
            this.context = context;
            this.productName = productName;
            this.productPrice = productPrice;
            this.productQuantity = productQuantity;
            this.productDescription=productDescription;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getEditProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                JSONObject jsonObject = new JSONObject(message);
                if(jsonObject.getString("edit").toString().equals("success"))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_Product_updated_successfully, Toast.LENGTH_SHORT);
                /*productList.set(position, productName);
                priceList.set(position, productPrice);
                quantityList.set(position, productQuantity);*/

                    Master.productList.set(position, productName);
                    Master.priceList.set(position, productPrice);
                    Master.quantityList.set(position, productQuantity);
                    Master.descList.set(position,productDescription);

                    mAdapter.notifyItemChanged(position);
                }
                else
                {
                    Master.alertDialog(ProductActivity.this, getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                //Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                Toast.makeText(ProductActivity.this, R.string.label_toast_data_send_failed, Toast.LENGTH_SHORT).show();
                Log.d("EXCEPTION","JSON EXCEPTION");
            }

        }
}
//---------------------End of EditProduct Asynctask-------------------------------------------------

//---------------------DeleteProduct Asynctask---------------------------------------------------------
    /******************************************************************
     API:
     /api/product/delete

     body:
     {
        "orgabbr":"Test2",
        "name":"Pumpkin",
     }

     response:
     {
        "result": "Delete successful"
            OR
        "result": "failed to delete"
     }
     *******************************************************************/
    public class DeleteProductTask extends AsyncTask<JSONObject, String, String>
    {
        Context context;
        String response, productName;
        ProgressDialog pd;
        int position;

        DeleteProductTask(Context context, String productName, int position)
        {
            this.context = context;
            this.productName = productName;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDeleteProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                JSONObject jsonObject = new JSONObject(message);
                if(jsonObject.getString("result").equals("Delete successful"))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_product_deleted_successfully, Toast.LENGTH_SHORT);
                    /*productList.remove(position);
                    priceList.remove(position);
                    quantityList.remove(position);*/

                    Master.productList.remove(position);
                    Master.priceList.remove(position);
                    Master.quantityList.remove(position);
                    Master.descList.remove(position);

                    mAdapter.notifyItemRemoved(position);
                    recyclerViewIndex--;
                    if(recyclerViewIndex <= 0)
                    {
                        alertDialog();
                    }
                }
                else
                {
                    Master.alertDialog(ProductActivity.this, getString(R.string.label_toast_cannot_delete_product), getString(R.string.label_alertdialog_ok));
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
            }

        }
    }
//---------------------End of DeleteProduct Asynctask-------------------------------------------------

    public class DisableEnableProductTask extends AsyncTask<JSONObject, String, String>
    {
        Context context;
        String response, productId;
        ProgressDialog pd;
        int position;
        String status;

        DisableEnableProductTask(Context context, String productId, int position, String status)
        {
            this.context = context;
            this.productId = productId;
            this.position = position;
            this.status = status;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDisableEnableProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                JSONObject jsonObject = new JSONObject(message);


                message=jsonObject.getString("response");


                if(message.equals("Success"))
                {
                    int i = jsonObject.getInt("allProductsDisabled");
                    String stat = String.valueOf(Master.productTypeDisplayList.get(activity_position).productTypeStatus);
                    Master.productTypeDisplayList.get(activity_position).productTypeStatus=i;

                    if(Integer.parseInt(stat)!= i){
                        Toast.makeText(ProductActivity.this, R.string.label_toast_producttype_status_changed, Toast.LENGTH_SHORT).show();
                    }

                    if(status.equals("0")){
                        Toast.makeText(context,getString(R.string.label_toast_product_enabled),Toast.LENGTH_SHORT);
                        Master.productStatusList.set(position,"1");
                        mAdapter.notifyItemChanged(position);
                    }
                    else{
                        Toast.makeText(context,getString(R.string.label_toast_product_disabled),Toast.LENGTH_SHORT);
                        Master.productStatusList.set(position,"0");
                        mAdapter.notifyItemChanged(position);
                    }
                }
                else {

                }
            }
            catch (JSONException e)
            {
                Master.alertDialog(ProductActivity.this, getString(R.string.label_toast_product_error_changing_status), getString(R.string.label_alertdialog_ok));
                e.printStackTrace();
            }

        }
    }
//---------------------End of DeleteProduct Asynctask-------------------------------------------------






    //---------------------AddProduct Asynctask---------------------------------------------------------
    public class AddProductTask extends AsyncTask<JSONObject, String, String>
    {
        Context context;
        String response, productName, productPrice, productQuantity,productDescription;
        ProgressDialog pd;

        AddProductTask(Context context, String productName, String productPrice, String productQuantity,String productDescription)
        {
            this.context = context;
            this.productName = productName;
            this.productPrice = productPrice;
            this.productQuantity = productQuantity;
            this.productDescription=productDescription;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getAddNewProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
               // System.out.println("add product response--------"+message);
                JSONObject jsonObject = new JSONObject(message);
                if(jsonObject.getString("upload").equals("success"))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_Product_added_successfully, Toast.LENGTH_SHORT);
                    /*productList.add(recyclerViewIndex, productName);
                    priceList.add(recyclerViewIndex, productPrice);
                    quantityList.add(recyclerViewIndex, productQuantity);*/

                    Master.productList.add(recyclerViewIndex, productName);
                    Master.priceList.add(recyclerViewIndex, productPrice);
                    Master.quantityList.add(recyclerViewIndex, productQuantity);
                    Master.descList.add(recyclerViewIndex,productDescription);
                    Master.productIdList.add(recyclerViewIndex,jsonObject.getString("id"));

                    Master.productStatusList.add(recyclerViewIndex,  String.valueOf(Master.productTypeDisplayList.get(activity_position).productTypeStatus));

                    //product status is at 1 but should be changed to the current status of the product type
                    //Log.d("Product type : ", productType);
                    //Log.d("Product type status: ", String.valueOf(Master.productTypeDisplayList.get(activity_position).productTypeStatus));


                    if(recyclerViewIndex == 0) // recycler view not initialised
                    {
                       // System.out.println("recyclerview index-if--0----------"+recyclerViewIndex);
                        //mAdapter = new ProductRecyclerViewAdapter(productList, quantityList, priceList, ProductActivity.this);

                        mAdapter = new ProductRecyclerViewAdapter(Master.productList, Master.quantityList, Master.priceList, Master.productStatusList,ProductActivity.this);

                        mRecyclerView.setAdapter(mAdapter);
                        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ProductActivity.this, mRecyclerView, Master.productClickKey, null));
                    }
                    else
                    {
                       // System.out.println("recyclerview index-----------"+recyclerViewIndex);

                        mAdapter.notifyItemInserted(recyclerViewIndex++);
                    }
                    dialog.dismiss();
                }
                else
                {
                    Master.alertDialog(ProductActivity.this, getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                //Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                Toast.makeText(ProductActivity.this,R.string.label_toast_data_send_failed, Toast.LENGTH_SHORT).show();
                Log.d("EXCEPTION","JSON EXCEPTION");
            }

        }
    }
//--------------------------End of AddProductTask -----------------------------------------------


}
