package admin.lokacart.ict.mobile.com.adminapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import admin.lokacart.ict.mobile.com.adminapp.fragments.*;

/**
 * Created by Vishesh on 13-01-2016.
 */
public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener{
    MyListener callback;
    int cat;
    Object obj;

    private GestureDetector mGestureDetector;

    public RecyclerItemClickListener(Context context,final RecyclerView recyclerView, final int cat, final Object obj)
    {
        callback = (MyListener) context;
        this.cat = cat;
        this.obj= obj;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener()
        {
            @Override
            public boolean onSingleTapUp(MotionEvent e)
            {
                return true;
            }
            @Override
            public void onLongPress(MotionEvent e)
            {
                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if(childView != null)
                {
                    DashboardActivity.resetBackPress();
                    callback.onCardLongClickListener(recyclerView.getChildAdapterPosition(childView), cat, obj);
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent e)
    {
        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if(childView != null && mGestureDetector.onTouchEvent(e))
        {
            DashboardActivity.resetBackPress();
            callback.onCardClickListener(recyclerView.getChildAdapterPosition(childView), cat, obj);
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

//    @Override
//    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//    }
//

}
