package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by ALCHEMi$T on 14-04-2016.
 */import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHAHasher {
    public static String getSHAHasher(String input) {
        try {
            System.out.println("Inside SHA Hasher");
            MessageDigest md = MessageDigest.getInstance("SHA");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        }
    }
