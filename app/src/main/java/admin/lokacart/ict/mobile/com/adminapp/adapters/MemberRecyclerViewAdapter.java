package admin.lokacart.ict.mobile.com.adminapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import admin.lokacart.ict.mobile.com.adminapp.ItemTouchHelperAdapter;
import admin.lokacart.ict.mobile.com.adminapp.Member;
import admin.lokacart.ict.mobile.com.adminapp.MyListener;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by Vishesh on 10-02-2016.
 */
public class MemberRecyclerViewAdapter extends RecyclerView.Adapter<MemberRecyclerViewAdapter.DataObjectHolder> implements ItemTouchHelperAdapter
{
    Context context;
    List<Member> orderList = new ArrayList<Member>();

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        TextView tMemberName, tID;
        MyListener callback;
        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            tMemberName = (TextView) itemView.findViewById(R.id.tMemberName);
            tID = (TextView) itemView.findViewById(R.id.tID);
            callback = (MyListener) context;
        }
    }

    public MemberRecyclerViewAdapter(ArrayList<Member> myDataset, Context context)
    {
        orderList = myDataset;
        this.context = context;
    }


    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.member_card, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view, context);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        Member member = orderList.get(position);
        String m = member.getName();
        String r = member.getRole();
        /*
        if(member.isA())
            m += " A";
        if(member.isM())
            m += " M";
        if(member.isP())
            m += " P";
        */
        try{
            holder.tMemberName.setText(m);
            holder.tID.setText(member.getUserID());
            if(r.equals("Admin")){
                Log.d("Error","Admin Displayed");
            }
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int position) {
        if(position<getItemCount()) {
            orderList.remove(position);
            notifyItemRemoved(position);
        }
    }

}
