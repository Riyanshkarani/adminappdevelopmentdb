package admin.lokacart.ict.mobile.com.adminapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by Vishesh on 15-01-2016.
 */
public class OrderDetailsRecyclerViewAdapter extends RecyclerView.Adapter<OrderDetailsRecyclerViewAdapter.DataObjectHolder>
{
    static String LOG_TAG = "OrderDetailsRecyclerViewAdapter";
    //  ArrayList<String> productList, productQuantityList, productPriceList;
    Context context;

    List<String[]> orderDetailsList = new ArrayList<String[]>();

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        TextView quantityName,rate,quantity;
        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            quantityName = (TextView) itemView.findViewById(R.id.quantityName);
            rate = (TextView) itemView.findViewById(R.id.rate);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
        }
    }

    public OrderDetailsRecyclerViewAdapter(ArrayList<String[]> myDataset, Context context)
    {
        orderDetailsList = myDataset;

        this.context = context;

    }


    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.orders_details_list_item_card_view, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view, context);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        String[] list = (String[])orderDetailsList.get(position);
        holder.quantity.setText(list[2]);
        holder.rate.setText(list[1]);
        holder.quantityName.setText(list[0]);

    }

    @Override
    public int getItemCount() {
        return orderDetailsList.size();
    }

}
