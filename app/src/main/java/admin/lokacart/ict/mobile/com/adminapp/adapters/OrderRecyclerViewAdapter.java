package admin.lokacart.ict.mobile.com.adminapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import admin.lokacart.ict.mobile.com.adminapp.MyListener;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.SavedOrder;

/**
 * Created by Vishesh on 15-01-2016.
 */
public class OrderRecyclerViewAdapter extends RecyclerView.Adapter<OrderRecyclerViewAdapter.DataObjectHolder>
{
    static String LOG_TAG = "OrderRecyclerViewAdapter";
    private final String tag;
    //  ArrayList<String> productList, productQuantityList, productPriceList;
    Context context;
    List<SavedOrder> orderList = new ArrayList<SavedOrder>();
    Boolean paymentStatus;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        TextView orderid,username, date, payment, deldate;
        MyListener callback;
        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            orderid = (TextView) itemView.findViewById(R.id.order_Id);
            username = (TextView) itemView.findViewById(R.id.username);
            date = (TextView) itemView.findViewById(R.id.date);
            payment=(TextView) itemView.findViewById(R.id.paymentStatus);
            //deldate = (TextView) itemView.findViewById(R.id.delDate);
            callback = (MyListener) context;
        }
    }

    public OrderRecyclerViewAdapter(ArrayList<SavedOrder> myDataset, Context context, String tag, Boolean paymentStatus)
    {
        orderList = myDataset;
        this.context = context;
        this.paymentStatus=paymentStatus;
        this.tag = tag;
    }


    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.orders_list_item_card_view, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view, context);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        SavedOrder order = orderList.get(position);
        holder.orderid.setText("" + order.getOrderId());
        holder.username.setText(order.getUserName());

        if(paymentStatus){
            holder.payment.setTextColor(Color.GRAY);
            if(order.getPayment().equals("true")){
                holder.payment.setText(R.string.textview_paid);
            }
            else
            {
                holder.payment.setText(R.string.textview_unpaid);
                holder.payment.setTextColor(Color.RED);
            }

        }else{
            holder.payment.setVisibility(View.GONE);
        }

        Date time = null;
        try {
            time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(order.getTimeStamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date = new SimpleDateFormat("EEE, MMM d, yyyy").format(time);
        //holder.date.setText("Order Date: "+date);
        holder.date.setText(date);
        /*String deld = order.getDelDate();
        Log.d("del Date",deld+"   "+order.getOrderId()+"   "+tag);


        switch (deld){
            case "not found":
                Log.d("Delivery Time","no deld found must be processed from old app   "+tag);
                holder.deldate.setTypeface(null,Typeface.BOLD_ITALIC);
                holder.deldate.setTextColor(Color.RED);
                holder.deldate.setText("Delivery Date: date not set");
                break;
            case "saved":
                Log.d("Delivery Time", "no deld for PLACED");
                break;
            case "cancelled":
                Log.d("Delivery Time","no deld for CANCEL");
            default:
                try {
                    Date delTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(deld);
                    deld = new SimpleDateFormat("EEE, MMM d, yyyy").format(delTime);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                holder.deldate.setTypeface(null, Typeface.BOLD);
                holder.deldate.setText("Delivery Date: " + deld);
        }
        if(tag == "PlacedOrderFragment" || tag == "CancelledOrderFragment"){
            holder.deldate.setVisibility(View.GONE);
        }*/

     /*   if(deld.equals("not found")){
            Log.d("Delivery Time","no deld found must be processed from old app   "+tag);
        }
        else if (deld.equals("saved")) {
            Log.d("Delivery Time", "no deld for placed");
        }
        else if (deld.equals("cancelled")){
            Log.d("Delivery Time", "no deld for CANCEL");
        }
        else {
            *//*Date delTime = null;*//*
            try {
                Date delTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(deld);
                deld = new SimpleDateFormat("EEE, MMM d, yyyy").format(delTime);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            holder.deldate.setTypeface(null, Typeface.BOLD);
            holder.deldate.setText("Delivery Date: " + deld);
        }
        if(tag == "PlacedOrderFragment" || tag == "CancelledOrderFragment"){
            holder.deldate.setVisibility(View.GONE);
        }*/
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

}