package admin.lokacart.ict.mobile.com.adminapp.adapters;

/**
 * Created by Vishesh on 19-01-2016.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import admin.lokacart.ict.mobile.com.adminapp.fragments.BillLayoutFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.DeliveredOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.GeneralSettingsFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.PlacedOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.ProfileFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.ProcessedOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.CancelledOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.PendingRequestFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.ExistingUserFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    PlacedOrderFragment savedOrderFragmentTab;
    ProcessedOrderFragment processedOrderFragmentTab;
    CancelledOrderFragment cancelledOrderFragmentTab;
    ProfileFragment profileFragmentTab;
    BillLayoutFragment billLayoutFragmentTab;
    PendingRequestFragment pendingRequestFragmentTab;
    ExistingUserFragment existingUserFragmentTab;
    GeneralSettingsFragment generalSettingsFragmentTab;
    DeliveredOrderFragment deliveredOrderFragmentTab;
    FragmentManager fm;
    String category;
   // FragmentManager fm = getSupportFragmentManager();

    public ExistingUserFragment getExistingUserFragmentTab()
    {
        return existingUserFragmentTab;
    }

    public PagerAdapter(FragmentManager fm, int NumOfTabs,String category) {
        super(fm);
        this.fm = fm;
        this.mNumOfTabs = NumOfTabs;
        this.category = category;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                if(category.equals("Setting"))
                {
                    profileFragmentTab = new ProfileFragment();
                    return  profileFragmentTab;
                }
                else if(category.equals("Order"))
                {
                    savedOrderFragmentTab = new PlacedOrderFragment();
                    return  savedOrderFragmentTab;
                }
                else if(category.equals("Member"))
                {
                    pendingRequestFragmentTab = new PendingRequestFragment();
                    return pendingRequestFragmentTab;
                }
                break;

            case 1:

                if(category.equals("Setting"))
                {
                    billLayoutFragmentTab = new BillLayoutFragment();
                    return  billLayoutFragmentTab;
                }
                else if(category.equals("Order"))
                {
                    processedOrderFragmentTab = new ProcessedOrderFragment();
                    return  processedOrderFragmentTab;
                }
                else if(category.equals("Member"))
                {
                    existingUserFragmentTab = new ExistingUserFragment();
                    return existingUserFragmentTab;
                }
                break;

            case 2:
                if(category.equals("Order"))
                {
                    deliveredOrderFragmentTab=new DeliveredOrderFragment();
                    return deliveredOrderFragmentTab;

                }
                else if(category.equals("Setting"))
                {
                    generalSettingsFragmentTab = new GeneralSettingsFragment();
                    return generalSettingsFragmentTab;
                }
                break;

            case 3:
                if(category.equals("Order"))
                {
                    cancelledOrderFragmentTab = new CancelledOrderFragment();
                    return cancelledOrderFragmentTab;
                }
                break;

            default:
                return null;
        }
        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}