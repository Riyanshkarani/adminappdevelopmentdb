package admin.lokacart.ict.mobile.com.adminapp.containers;

/**
 * Created by madhav on 16/6/16.
 */

import org.json.JSONException;
import org.json.JSONObject;
public class Product {

    private String audioUrl;
    private double unitPrice;
    private double total;
    private int quantity;
    private String stockEnabledStatus;
    private int stockQuantity;
    private String name;
    private String imageUrl;
    private String orgAbbr;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    private String ID;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    private String Description;
   /* public double getDquantity() {
        return dquantity;
    }

    public void setDquantity(double dquantity) {
        this.dquantity = dquantity;
    }

    private double dquantity;*/

    public String getOrgAbbr() {
        return orgAbbr;
    }

    public void setOrgAbbr(String orgAbbr) {
        this.orgAbbr = orgAbbr;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStockEnabledStatus() {
        return stockEnabledStatus;
    }

    public void setStockEnabledStatus(String stockEnabledStatus) {
        this.stockEnabledStatus = stockEnabledStatus;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product(String name, double unitPrice, double total, int stockQuantity, String imageUrl, String audioUrl, String Description,String ID) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.total = total;
        this.quantity = (int) (total/unitPrice);
        this.stockQuantity = stockQuantity;
        this.imageUrl = imageUrl;
        this.audioUrl = audioUrl;
        this.Description = Description;
        this.ID=ID;
    }

    public Product(String name, double unitPrice, double total) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.total = total;
        this.quantity = (int)(total/unitPrice);
    }

    public Product(String name, int quantity, Double unitPrice, Double total) {
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.total = total;
    }


    public Product(String name, int quantity, int stockQuantity,Double unitPrice, Double total) {
        this.name = name;
        this.quantity = quantity;
        this.stockQuantity=stockQuantity;
        this.unitPrice = unitPrice;
        this.total = total;
    }


    /*public Product(String name, double quantity, Double unitPrice, Double total) {
        this.name = name;
        this.dquantity = quantity;
        this.unitPrice = unitPrice;
        this.total = total;
    }
*/


    public Product(JSONObject orderItem) throws JSONException {
        this.name = orderItem.getString("name");
        this.unitPrice = Double.parseDouble(orderItem.getString("rate"));
        this.quantity = Integer.parseInt(orderItem.getString("quantiity"));
        this.total = this.quantity * this.unitPrice;
    }

    public Product()
    {

    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
