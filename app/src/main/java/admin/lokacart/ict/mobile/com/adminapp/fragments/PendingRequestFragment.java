package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.Member;
import admin.lokacart.ict.mobile.com.adminapp.adapters.MemberRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;

/**
 * Created by Vishesh on 08-02-2016.
 */
public class PendingRequestFragment extends Fragment{

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    protected LayoutManagerType mCurrentLayoutManagerType;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    ProgressDialog pd;
    String response;
    ArrayList<Member> memberArrayList, members;
    ArrayList<JSONObject> memberObjects;
    Dialog dialog;
    private SwipeRefreshLayout swipeContainer;
    private int count = 0;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    JSONObject responseObject;
    static int recyclerViewIndex;
    View pendingRequestFragmentView;
    TextView tMember;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        pendingRequestFragmentView = inflater.inflate(R.layout.fragment_member_recycler_view, container, false);
        getActivity().setTitle(R.string.title_members);
        memberArrayList = new ArrayList<Member>();

        tMember = (TextView) pendingRequestFragmentView.findViewById(R.id.tMember);

        swipeContainer = (SwipeRefreshLayout) pendingRequestFragmentView.findViewById(R.id.memberSwipeToRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetPendingRequestsTask(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);

        return pendingRequestFragmentView;
    }

    public ArrayList<Member> getMembers() {

        members = new ArrayList<Member>();
        if(memberObjects.size()!=0)
        {
            for(JSONObject entry : memberObjects){
                Member member = new Member(entry);
                members.add(member);
            }
        }
        else
        {

        }
        return members;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        mRecyclerView = (RecyclerView) pendingRequestFragmentView.findViewById(R.id.memberRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        new GetPendingRequestsTask(true).execute();

        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }

    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        if(layoutManagerType != null)
        {
            switch (layoutManagerType)
            {
                case LINEAR_LAYOUT_MANAGER:
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                    break;
                default:
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
            }
        }


        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    public void clickListener(final int position)
    {
        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.member_box);
        dialog.setTitle("Details of " + memberArrayList.get(position).getName());
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eName, eLName, eEmail, ePhone, eAddress, ePincode;
        final Button bEdit, bConfirm, bCancel, bApprove, bReject;
        final CheckBox cAdmin, cPublisher, cMember;
        final TextView tRole;

        tRole = (TextView) dialog.findViewById(R.id.tRole);
        tRole.setVisibility(View.GONE);

        eName = (EditText) dialog.findViewById(R.id.eMemberFirstName);
        eName.setText(memberArrayList.get(position).getName());
        eName.setEnabled(false);

        eLName = (EditText) dialog.findViewById(R.id.eMemberLastName);
        eLName.setText(memberArrayList.get(position).getLastName());
        eLName.setEnabled(false);

        eEmail = (EditText) dialog.findViewById(R.id.eMemberEmail);
        eEmail.setText(memberArrayList.get(position).getEmail());
        eEmail.setEnabled(false);

        ePhone = (EditText) dialog.findViewById(R.id.eMemberPhone);
        ePhone.setText(memberArrayList.get(position).getPhone());
        ePhone.setEnabled(false);

        eAddress = (EditText) dialog.findViewById(R.id.eMemberAddress);
        eAddress.setText(memberArrayList.get(position).getAddress());
        eAddress.setEnabled(false);

        ePincode = (EditText) dialog.findViewById(R.id.eMemberPincode);
        ePincode.setText(memberArrayList.get(position).getPincode());
        ePincode.setEnabled(false);

        cAdmin = (CheckBox) dialog.findViewById(R.id.cAdmin);
        cAdmin.setVisibility(View.GONE);

        cPublisher = (CheckBox) dialog.findViewById(R.id.cPublisher);
        cPublisher.setVisibility(View.GONE);

        cMember = (CheckBox) dialog.findViewById(R.id.cMember);
        cMember.setVisibility(View.GONE);

        bEdit = (Button) dialog.findViewById(R.id.bMemberEdit);
        bEdit.setVisibility(View.GONE);
        bConfirm = (Button) dialog.findViewById(R.id.bMemberConfirm);
        bConfirm.setVisibility(View.GONE);

        bApprove = (Button) dialog.findViewById(R.id.bMemberApprove);
        bApprove.setVisibility(View.VISIBLE);
        bReject = (Button) dialog.findViewById(R.id.bMemberReject);
        bReject.setVisibility(View.VISIBLE);
        bCancel = (Button) dialog.findViewById(R.id.bMemberCancel);

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try
                {
                    jsonObject.put("orgabbr", AdminDetails.getAbbr());
                    jsonObject.put("userId", memberArrayList.get(position).getIntUserID());
                    new ApproveMemberTask(position, dialog).execute(jsonObject);
                }
                catch (Exception e)
                {
                }
            }
        });

        bReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try
                {
                    jsonObject.put("orgabbr", AdminDetails.getAbbr());
                    jsonObject.put("userId", memberArrayList.get(position).getIntUserID());
                    new RejectMemberTask(position, dialog).execute(jsonObject);
                }
                catch (Exception e)
                {
                }
            }
        });
    }

    public class GetPendingRequestsTask extends AsyncTask<Void, String, String>
    {
        String response;
        ProgressDialog pd;
        Boolean showProgressDialog;

        public GetPendingRequestsTask(Boolean showProgressDialog)
        {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            if(showProgressDialog)
            {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.label_please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Void... params)
        {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getPendingRequestsURL(AdminDetails.getAbbr()), null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message)
        {
            if(showProgressDialog)
            {   if(pd != null && pd.isShowing())
                pd.dismiss();
            }

            if(PendingRequestFragment.this.isAdded()){
                if (response.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                }
                else
                {
                    try
                    {

                        memberObjects= new ArrayList<JSONObject>();
                        responseObject = new JSONObject(response);
                        JSONArray jsonArray = responseObject.getJSONArray("users");
                        if(jsonArray.length() == 0)
                        {
                            tMember.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            count++;
                            tMember.setVisibility(View.GONE);
                            for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                            {
                                memberObjects.add((JSONObject)jsonArray.get(recyclerViewIndex));
                            }
                            memberArrayList = getMembers();
                            mAdapter = new MemberRecyclerViewAdapter(memberArrayList, getActivity());
                            if(count < 2)
                            {
                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.pendingUserRequestKey, PendingRequestFragment.this));
                                mAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                mRecyclerView.swapAdapter(mAdapter,true);
                            }
                        }
                        swipeContainer.setRefreshing(false);
                    }
                    catch (Exception e)
                    {

                    }
                }
            }

        }
    }

    public class ApproveMemberTask extends AsyncTask<JSONObject, String, String>
    {
        int position;
        Dialog d;

        public ApproveMemberTask(int pos, Dialog dialog)
        {
            position = pos;
            d = dialog;
        }
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.pd_approving_member));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getApproveURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            pd.dismiss();
            if(message.equals("exception"))
            {
                Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
            }
            else
            {
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(message);
                    if(jsonObject.get("response").equals("Member Approved"))
                    {
                        Toast.makeText(getActivity(), R.string.label_toast_request_approved_successfully, Toast.LENGTH_SHORT).show();
                        memberArrayList.remove(position);
                        mAdapter.notifyItemRemoved(position);
                        recyclerViewIndex--;
                        d.dismiss();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }



//------------------------------------------------------------------------------------------------------------------//
    private class RejectMemberTask extends AsyncTask<JSONObject, String, String>
    {
        int position;
        Dialog d;

        public RejectMemberTask(int pos, Dialog dialog)
        {
            position = pos;
            d = dialog;
        }
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.pd_rejecting_member));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getRejectURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            pd.dismiss();
            if(message.equals("exception"))
            {
                Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
            }
            else
            {
                try
                {
                    JSONObject jsonObject = new JSONObject(message);
                    if(jsonObject.get("response").equals("Member Removed"))
                    {
                        Toast.makeText(getActivity(), R.string.label_toast_request_rejected_successfully, Toast.LENGTH_SHORT).show();
                        memberArrayList.remove(position);
                        mAdapter.notifyItemRemoved(position);
                        recyclerViewIndex--;
                        d.dismiss();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
