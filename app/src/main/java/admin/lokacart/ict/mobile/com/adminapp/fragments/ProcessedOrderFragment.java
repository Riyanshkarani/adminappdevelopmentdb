package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.adapters.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.SavedOrder;

/**
 * Created by Vishesh on 19-01-2016.
 */
public class ProcessedOrderFragment extends Fragment  {

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private SwipeRefreshLayout swipeContainer;
    protected LayoutManagerType mCurrentLayoutManagerType;
    private enum LayoutManagerType { GRID_LAYOUT_MANAGER, LINEAR_LAYOUT_MANAGER }
    public static ArrayList<SavedOrder> processedOrderArrayList;
    View processedOrderFragmentView,enterAmountOnDelivery;
    RecyclerView mRecyclerView;
    static RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Master master;
    JSONObject responseObject;
    static int recyclerViewIndex;
    ArrayList<JSONObject> orderObjects;
    ArrayList<SavedOrder> orders;
    private int count=0;
    TextView  tOrders;
    private int apiCalls;
    EditText eAmountReceived;

    public static final String TAG = "ProcessedOrderFragment";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        processedOrderFragmentView = inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);

        return processedOrderFragmentView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        processedOrderArrayList = new ArrayList<SavedOrder>();
    }

    public ArrayList<SavedOrder> getOrders() {
        orders = new ArrayList<SavedOrder>();
        int count=0;
        if(orderObjects.size()!=0)
        {
            for(JSONObject entry : orderObjects){
                SavedOrder order = new SavedOrder(entry,count,Master.PROCESSEDORDER);
                orders.add(order);
                count++;
            }
        }
        else
        {


        }
        return orders;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        new GetProcessedOrderDetails(true).execute();

        mRecyclerView = (RecyclerView) processedOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        swipeContainer = (SwipeRefreshLayout) processedOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetProcessedOrderDetails(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        tOrders = (TextView) processedOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_processed_orders_present);
        tOrders.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }


    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {

            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void clickListener(int position)
    {
        new ViewBillAsyncTask(getActivity(),position).execute("" + processedOrderArrayList.get(position).getOrderId());
    }

    public class ViewBillAsyncTask extends AsyncTask<String,String,String>
    {
        Context context;
        ProgressDialog pd;
        int position;

        ViewBillAsyncTask(Context context,int position) {
            this.context = context;
            this.position=position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String status;
            HttpURLConnection linkConnection = null;
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String emailid = sharedPreferences.getString("emailid","test2@gmail.com");
            String password =  sharedPreferences.getString("password","password");

            try {

                //URL linkurl = new URL("http://ruralict.cse.iitb.ac.in/ruralict/api/"+AdminDetails.getAbbr()+"/viewbill/"+params[0]);
                URL linkurl = new URL("http://best-erp.com:443/ruralict/api/"+AdminDetails.getAbbr()+"/viewbill/"+params[0]);
                linkConnection = (HttpURLConnection) linkurl.openConnection();
                String basicAuth = "Basic " + new String(Base64.encode((emailid + ":" + password).getBytes(), Base64.NO_WRAP));
                linkConnection.setRequestProperty("Authorization", basicAuth);
                linkConnection.setDefaultUseCaches(false);
                linkConnection.setRequestMethod("GET");
                linkConnection.setRequestProperty("Accept", "text/html");
                linkConnection.setDoInput(true);
                InputStream is = null;
                status=String.valueOf(linkConnection.getResponseCode());
                if(status.equals("200"))
                {
                    is=linkConnection.getInputStream();
                }
                else
                {
                    status="exception";
                    return  status;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                status = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
                status="exception";
                return status;
            }
            finally {
                if (linkConnection != null) {
                    linkConnection.disconnect();
                }
            }
            return status;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(pd != null && pd.isShowing())
            pd.dismiss();

            if(ProcessedOrderFragment.this.isAdded()){
                if (s.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                }
                else if(s.equals("failure")){
                    Toast.makeText(getActivity(),R.string.label_toast_order_already_processed,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle(R.string.dialog_title_bill);
                    WebView wv = new WebView(context);
                    wv.loadData(s, "text/html", "UTF-8");
                    wv.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                    });
                    alert.setView(wv);

                    alert.setNegativeButton(R.string.label_alertdialog_delivered, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AlertDialog.Builder confirm_alert = new AlertDialog.Builder(context);
                            //enterAmountOnDelivery=getActivity().getLayoutInflater().inflate(R.layout.amount_received_at_delivery_dialog,null);
                            //confirm_alert.setView(enterAmountOnDelivery);
                            confirm_alert.setTitle(R.string.title_alertdialog_are_you_sure_order_is_delivered);

                            //eAmountReceived=(EditText) enterAmountOnDelivery.findViewById(R.id.eAmountReceived);
                            confirm_alert.setPositiveButton(getString(R.string.label_alertdialog_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    //if (!eAmountReceived.getText().toString().trim().isEmpty()) {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("status", "delivered");
                                            //For sending the current date to match it with the one in database to confirm which is the real delivery date

                                            /*Date date = new Date();
                                            Timestamp ts = new Timestamp(date.getTime());
                                            jsonObject.put("deldate",String.valueOf(ts));*/

                                            //jsonObject.put("amoount",eAmountReceived.toString());
                                        } catch (JSONException e) {
                                        }
                                        new ChangeToDeliveredOrderTask(true, processedOrderArrayList.get(position).getOrderId(), position).execute(jsonObject);
                                   // }
                                   // else{
                                   //     Toast.makeText(getActivity(),getString(R.string.label_toast_no_amount_entered),Toast.LENGTH_SHORT).show();
                                   // }
                                }
                            });
                            confirm_alert.setNegativeButton(getString(R.string.label_button_cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            confirm_alert.show();

                        }
                    });
                    alert.setPositiveButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
            }

        }
    }



    public class GetProcessedOrderDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        Boolean showProgressDialog;
        String response;

        public GetProcessedOrderDetails(Boolean showProgressDialog) {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.pd_loading_orders));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String processedOrderURL = master.getProcessedOrderURL() + "?orgabbr=" + AdminDetails.getAbbr();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(processedOrderURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            //Log.d("JSON Output proc",response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (showProgressDialog)
                if(pd != null && pd.isShowing())
                    pd.dismiss();

            if (response.equals("exception")) {
                Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
            } else {
                try {
                    orderObjects = new ArrayList<JSONObject>();
                    responseObject = new JSONObject(response);
                    JSONArray jsonArray = responseObject.getJSONArray("orders");
                    if (jsonArray.length() == 0) {
                        tOrders.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tOrders.setVisibility(View.GONE);
                        ++count;
                        for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex) {
                            orderObjects.add((JSONObject) jsonArray.get(recyclerViewIndex));
                        }
                        processedOrderArrayList = getOrders();
                        mAdapter = new OrderRecyclerViewAdapter(processedOrderArrayList, getActivity(), TAG,false);
                        if (count < 2) {
                            mRecyclerView.setAdapter(mAdapter);
                            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.processedOrderKey, ProcessedOrderFragment.this));
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mRecyclerView.swapAdapter(mAdapter, true);
                        }
                    }

                    swipeContainer.setRefreshing(false);

                } catch (Exception e) {

                }
            }
        }

    }

    public class ChangeToDeliveredOrderTask extends AsyncTask<JSONObject,String,String>{
        ProgressDialog pd;
        Boolean showProgressDialog;
        String response;
        int orderID;
        int position;

        public ChangeToDeliveredOrderTask(Boolean showProgressDialog,int orderID,int position) {
            this.showProgressDialog = showProgressDialog;
            this.orderID=orderID;
            this.position=position;
        }

        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.label_please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getChangeDeliveredState(orderID), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (showProgressDialog)
                if (pd != null && pd.isShowing())
                    pd.dismiss();

            if(ProcessedOrderFragment.this.isAdded()) {

                try {
                    responseObject = new JSONObject(response);
                    response = responseObject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (response.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                }
                else if(response.equals("Success")){
                    Toast.makeText(getActivity(),R.string.label_toast_status_changed_to_delivered,Toast.LENGTH_SHORT).show();

                    //Delete that order from the list
                    processedOrderArrayList.remove(position);
                    mAdapter.notifyItemRemoved(position);
                    recyclerViewIndex--;
                    if(recyclerViewIndex==0){
                        tOrders.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                    else {
                        tOrders.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }

                }
                else if(response.equals("error")){
                    try {
                        response=responseObject.getString("error");
                    }catch (JSONException e){

                    }
                    Toast.makeText(getActivity(),response.toString()+R.string.label_please_refresh,Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(getActivity(),R.string.label_toast_something_went_worng,Toast.LENGTH_SHORT).show();
                }
            }

            }



        }


    }



